import numpy as np
import xgboost as xgb
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from keras.models import Sequential
from keras.layers import Input, Dense, Dropout, LSTM, Merge, Conv1D, MaxPool1D, Concatenate, Flatten, GRU
from keras.models import Model
from keras.callbacks import ModelCheckpoint

class Model_(object):
    def __init__(self):
        pass

    def fit(self, X_train, y_train):
        pass

    def predict(self, X_test):
        pass

    def compute_score(self, y_test, y_pred):
        return np.sqrt(mean_squared_error(y_test, y_pred))


class SVR_Model(Model_):
    def __init__(self, kernel='rbf', degree=3, gamma='auto', coef0=0.0, tol=0.001, C=1.0, epsilon=0.1, shrinking=True,
                 cache_size=200, verbose=False, max_iter=-1):
        super().__init__()
        self.model = SVR(kernel='rbf', degree=3, gamma='auto', coef0=0.0, tol=0.001, C=1.0, epsilon=0.1, shrinking=True,
                         cache_size=200, verbose=False, max_iter=-1)

    def fit(self, Xtrain, Ytrain):
        self.model.fit(Xtrain, Ytrain)

    def predict(self, Xtest):
        return self.model.predict(Xtest)


class Neural_Net_Model(Model_):
    def __init__(self, input_dim, size_hidden=[64, 64], activation="tanh", loss='mse', optimizer='adam'):
        super().__init__()
        self.name_="NN"
        self.model = Sequential()
        self.model.add(Dense(units=size_hidden[0], activation=activation, kernel_initializer="normal", input_dim=input_dim))
        for s in size_hidden:
            self.model.add(Dense(units=s, activation=activation, kernel_initializer="normal"))
            self.model.add(Dropout(0.5))
        self.model.add(Dense(units=1))
        self.model.compile(loss=loss, optimizer=optimizer)
        self.model.save_weights('model.h5')

    def fit(self, X_train, y_train, validation_split=0.1, epochs=15, batch_size=128, verbose=2, sample_weight=None):
            checkpoint_best = ModelCheckpoint("weights.h5", monitor='val_loss', verbose=2, save_best_only=True, save_weights_only=True, mode='min')
            self.hist= self.model.fit(X_train, y_train, validation_split=validation_split, callbacks=[checkpoint_best], epochs=epochs, batch_size=batch_size, verbose=verbose, sample_weight=sample_weight)
            self.model.load_weights("weights.h5")
            return self.hist

    def predict(self, X_test):
        return self.model.predict(X_test)

    def reset_weight(self):
        self.model.load_weights('model.h5')
        return

class Gradient_Boosting(Model_):
    def __init__(self, loss="ls", learning_rate=0.1, n_estimators=100, max_depth=3, min_samples_split=2):
        super().__init__()
        self.name_ = "GB"
        self.model = GradientBoostingRegressor(loss=loss, learning_rate=learning_rate, n_estimators=n_estimators,
                                               max_depth=max_depth, min_samples_split=min_samples_split)

    def fit(self, X_train, y_train):
        self.model.fit(X_train, y_train)

    def predict(self, X_test):
        return self.model.predict(X_test)


class XGB(Model_):
    def __init__(self, booster='gbtree', objective='reg:linear', eta=1, max_depth=3, num_round=100):
        super().__init__()
        self.params = {'booster': booster, 'objective': objective, 'eta': eta, 'max_depth': max_depth, 'silent': 1}
        self.num_round = num_round
        self.name_="XGB"

    def fit(self, X_train, y_train):
        dtrain = xgb.DMatrix(X_train, label=y_train)
        self.model = xgb.train(self.params, dtrain, self.num_round)

    def predict(self, X_test):
        dtest = xgb.DMatrix(X_test, label=np.zeros(len(X_test)))
        return self.model.predict(dtest)

class LSTM_CNN(Model_):
    def __init__(self):
        super().__init__()
        self.name_= "LSTM_CNN"
        inp = Input(shape=(2000,1))

        # CNN conv 1D
        conv_1 = Conv1D(filters=64, kernel_size=1024, strides=1, activation='relu')(inp)
        drop_1 = Dropout(0.5)(conv_1)
        conv_2 = Conv1D(filters=64, kernel_size=512, strides=1, activation='relu')(drop_1)
        drop_2 = Dropout(0.5)(conv_2)
        max_pool_1 = MaxPool1D()(drop_2)
        conv_3 = Conv1D(filters=32, kernel_size=256, strides=1, activation='relu')(inp)
        drop_3 = Dropout(0.5)(conv_3)
        cnn_rep = MaxPool1D()(drop_3)
        cnn_rep_flat = Flatten()(cnn_rep)
        dense_im = Dense(128, activation='relu')(cnn_rep_flat)

        # LSTM
        lstm_1 = GRU(units=64)(inp)
        #lstm_out = LSTM(units=64, return_state=True)(lstm_1)

        #dense_inp = Concatenate()([dense_im, lstm_out[0], lstm_out[1]])
        dense_inp = Concatenate()([dense_im, lstm_1])
        dense_1 = Dense(128, activation='tanh')(dense_inp)
        output = Dense(1)(dense_1)

        self.model = Model(inputs=inp, outputs=output)
        self.model.compile(optimizer='rmsprop', loss='mse')
        self.model.save_weights('model_LSTM_CNN.h5')
        self.model.summary()

    def fit(self, X_train, y_train, validation_split=0, epochs=15, batch_size=128, verbose=1, sample_weight=None):
            self.hist= self.model.fit(X_train, y_train, validation_split=validation_split, epochs=epochs, batch_size=batch_size, verbose=verbose, sample_weight=sample_weight)
            self.model.save_weights('model_LSTM_CNN_trained.h5')
            return self.hist

    def predict(self, X_test):
        return self.model.predict(X_test)

    def reset_weight(self):
        self.model.load_weights('model_LSTM_CNN.h5')
        return



