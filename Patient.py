import numpy as np
import sys
import pickle
import pywt
from spectrum import arburg
from scipy.signal import periodogram
from sklearn.preprocessing import OneHotEncoder

class Record(object):
    def __init__(self, all_user_ids, user_id, night_id, eeg, resp_x, resp_y, resp_z, time, time_previous, number_previous, power_increase=None):
        self.all_user_ids = all_user_ids
        self.user_id = user_id
        self.night_id = night_id
        self.eeg = eeg
        self.resp_x = resp_x
        self.resp_y = resp_y
        self.resp_z = resp_z
        self.time = time
        self.time_previous = time_previous
        self.number_previous = number_previous
        self.power_increase = power_increase

    def extract_features(self, eeg_method="wavelet"):
        self.features = np.array([self.time, self.time_previous, self.number_previous])
        # EEG feature extraction

        # Extract features about end of eeg signal (stimulation is supposed to be better when up state of eeg)
        self.features = np.concatenate((self.features, [np.mean(self.eeg[-250:])], [np.mean(np.diff(self.eeg[-125:]))]))
        # Time frequency features computed using either FFT, periodogram or Wavelets
        sample_freq = 250
        len_windows = 1000
        over_lap = 500
        nb_windows = int((len(self.eeg) - len_windows) // over_lap + 1)
        windows_eeg = [self.eeg[k * over_lap:k * over_lap + len_windows] for k in range(nb_windows)]
        if eeg_method == "spectral":
            freq_bands = np.array([[0, 0.25],[0.25, 0.5], [0.5, 0.75], [0.75, 1],
                                   [1, 1.25], [1.25, 1.5], [1.5, 1.75], [1.75, 2],
                                   [2, 2.25], [2.25, 2.5], [2.5, 2.75], [2.75, 3],
                                   [3, 3.25], [3.25, 3.5], [3.5, 3.75], [3.75, 4],
                                   [4, 8], [8, 12]])
            eeg_features = np.array([])
            for window in windows_eeg[1:]:
                freq, spectral_density = periodogram(window, sample_freq)
                spectral_density_in_band = np.array([spectral_density[(band[0] <= freq) & (freq < band[1])].sum()/(band[1]-band[0]) for band in freq_bands])
                eeg_features = np.concatenate((eeg_features, spectral_density_in_band))
            self.features = np.concatenate((self.features, eeg_features))
        elif eeg_method == "wavelet":
            for window in windows_eeg[1:]:
                window_wt = pywt.wavedec(window, 'db4', level=6)
                eeg_features = np.array([(s**2).sum() for s in window_wt])
                self.features = np.concatenate((self.features, eeg_features))
        # Area under curve for EEG signal
        area_under_curve = np.array([np.trapz(np.abs(self.eeg))])

        self.features = np.concatenate((self.features, area_under_curve))
        # ZCP features
        zerocrossings = np.abs(np.diff(np.sign(np.sign(self.eeg - np.mean(self.eeg))))) / 2
        zcps_indexes = np.where(zerocrossings == 1)[0]
        zcps_indexes = np.concatenate((np.array([0]), zcps_indexes))
        zcp_lengths = np.diff(zcps_indexes)
        zcp_length_mean = np.mean(zcp_lengths)
        zcp_length_var = np.var(zcp_lengths)
        zcp_area = 0
        for i in range(zcps_indexes.shape[0] - 1):
            sub_signal = self.eeg[zcps_indexes[i]:zcps_indexes[i + 1]]
            zcp_area = (zcps_indexes[i + 1] - zcps_indexes[i]) * np.trapz(np.abs(sub_signal))
        self.features = np.concatenate((self.features, np.array([zcp_length_mean, zcp_length_var, zcp_area])))
        # Respiratory features
        respiratory_features = self.extract_respiratory_features(self.resp_z, 50)
        self.features = np.concatenate((self.features, respiratory_features))

        # User specific feature
        encoder = OneHotEncoder()
        self.all_user_ids = np.reshape(self.all_user_ids, (-1,1))
        
        
        encoder.fit(self.all_user_ids)
        user_feature = np.asarray(encoder.transform([[self.user_id]]).todense())[0]
        self.features = np.concatenate((self.features, user_feature))


    def extract_respiratory_features(self, resp_signal, fs):
        ei = np.mean(resp_signal ** 2)
        rf = np.sum(np.abs(np.diff(np.sign(resp_signal - np.sqrt(ei)))) / 2)
        mean = np.mean(resp_signal)
        var = np.var(resp_signal)
        resp_signal_1s_windows = [resp_signal[k*50:(k+1)*50] for k in range(8)]
        amp = np.array([np.max(resp_signal_1s) - np.min(resp_signal_1s) for resp_signal_1s in resp_signal_1s_windows])
        respiratory_features = np.concatenate((np.array([ei, rf, mean, var]), amp))
        #dwt_resp_sig = pywt.wavedec(resp_signal, 'db4', level=5)
        #dwt_resp_features = np.array([np.max(s**2) for s in dwt_resp_sig[1:])
        #ar_coeffs, _, _ =  arburg(resp_signal,2)
        #far = fs/(2*(np.pi))*np.arctan(ar_coeffs[0]/ar_coeffs[1])
        #str = np.sqrt(ar_coeffs[0]**2+ar_coeffs[1]**2)
        return respiratory_features



class Patient(object):
    def __init__(self, user_id, records, record_ids):
        self.userID = user_id
        self.records = records
        self.record_ids = record_ids

def create_dataset_train():
    with open("train_data", "rb") as input:
        data = pickle.load(input)
    X = []
    Y = []
    i = 1
    for user in data:
        for record in user.records.values():
            record.extract_features(eeg_method="spectral")
            x = record.features
            y = np.array([record.power_increase])
            X.append(x)
            Y.append(y)
        text = "\rUsers computed : {}% done".format(round(float(i) / len(data) * 100, 2))
        i += 1
        sys.stdout.write(text)
        sys.stdout.flush()

    X = np.array(X)
    Y = np.array(Y)
    with open("features_spectral_train", "wb") as file:
        pickle.dump([X,Y], file, pickle.HIGHEST_PROTOCOL)
    return

def create_dataset_test():
    with open("test_data", "rb") as input:
        data = pickle.load(input)
    X = []
    for record in data:
        record.extract_features(eeg_method="spectral")
        x = record.features
        X.append(x)
    X = np.array(X)
    with open("features_spectral_test", "wb") as file:
        pickle.dump(X, file, pickle.HIGHEST_PROTOCOL)
    return

if __name__ == "__main__":
    create_dataset_train()
    create_dataset_test()



