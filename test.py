import pickle
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from Models import *
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use("seaborn-whitegrid")

features = "features_spectral"


print("Reading data from features file.........")
with open(features + "_train" , "rb") as input_train:
    [X, Y] = pickle.load(input_train)

print("Dataset loaded")

input_dim = len(X[0])

#X =  np.expand_dims(X, axis=-1) 
print("Feature size : ", len(X[0]))

model = Neural_Net_Model(input_dim=input_dim, activation="sigmoid", size_hidden=[input_dim,input_dim])
#model = Gradient_Boosting(n_estimators=150, max_depth=3, min_samples_split=2)
#model = XGB(booster='gbtree', objective='reg:linear', eta=0.1, max_depth=6, num_round=150)
#model = LSTM_CNN()

def cross_validate(model, X, Y, k, plots=False):
    scores = []
    for i in range(k):
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)
        scaler = StandardScaler().fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)
        if model.name_ in ["NN", "LSTM_CNN"]:
            model.reset_weight()
            history = model.fit(X_train, y_train, validation_split=0.2, epochs=30, batch_size=128, verbose=2)
        elif model.name_ in ["GB", "XGB"]:
            y_train, y_test = y_train.flatten(), y_test.flatten()
            model.fit(X_train, y_train)
        y_pred = model.predict(X_test)

        # summarize history for loss
        if plots:
            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.title('model loss')
            plt.ylabel('loss')
            plt.xlabel('epoch')
            plt.legend(['train', 'val'], loc='upper left')
            plt.show()
        score = model.compute_score(y_test, y_pred)
        #compare(y_pred, y_test, 50)
        print("Score : ", score)
        scores.append(score)

    print("Average score on {} tests : {}".format(k, np.mean(scores)))

def compare(y_pred, y_test, k):
    print(np.mean(y_pred-y_test))
    for y_p, y_t in zip(y_pred[:k], y_test[:k]):
        print("Pred : ", y_p, "  True : ", y_t, "   ", y_p - y_t)
    return

def get_test_results(model):
    print("Loading data....")
    with open(features + "_test", "rb") as input_test:
        X_test = pickle.load(input_test)
    print(X_test.shape)
    with open(features + "_train", "rb") as input_train:
        [X_train, Y_train] = pickle.load(input_train)
    print("Data loaded")

    print("Fitting model .....")
    X_train, Y_train = shuffle(X_train, Y_train)
    scaler = StandardScaler().fit(np.concatenate([X_train, X_test]))
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    if model.name_ in ["NN", "LSTM_CNN"]:
            #X =  np.expand_dims(X, axis=-1)
            history = model.fit(X_train, Y_train, validation_split=0.25, epochs=35, batch_size=128, verbose=2)
    elif model.name_ in ["GB", "XGB"]:
        Y_train = Y_train.flatten()
        model.fit(X_train, Y_train)
    print("Model fitted.")
    print("Predicting labels")
    predicted_labels = model.predict(X_test)
    print("Writing output....")
    df = pd.DataFrame(predicted_labels)
    df.to_csv("test_output_deep_NN.csv", header=["power_increase"], index=True, index_label="index")
    return

get_test_results(model)
#cross_validate(model, X, Y, 10, plots=False)
