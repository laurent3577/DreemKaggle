import numpy as np
import pandas as pd
import pickle
import sys
from Patient import *

data_path = "data/test.csv"


def get_patients_train(df):
    list_patients = []
    users = np.unique(df['user'])
    i = 1
    for user in users:
        user_df = df.loc[df['user'] == user]
        record_ids = np.unique(user_df['Unnamed: 0'])
        records = {}
        for record_id in record_ids:
            user_record = user_df.loc[user_df['Unnamed: 0'] == record_id]
            index = user_record.index[0]
            eeg_cols = [col for col in user_record.columns if "eeg" in col]
            eeg = user_record[eeg_cols].values.flatten()
            resp_x_cols = [col for col in user_record.columns if "respiration_x" in col]
            resp_y_cols = [col for col in user_record.columns if "respiration_y" in col]
            resp_z_cols = [col for col in user_record.columns if "respiration_z" in col]
            resp_x = user_record[resp_x_cols].values.flatten()
            resp_y = user_record[resp_y_cols].values.flatten()
            resp_z = user_record[resp_z_cols].values.flatten()
            night_id = user_record.loc[index,'night']
            time = user_record.loc[index,'time']
            time_previous = user_record.loc[index,'time_previous']
            number_previous = user_record.loc[index,'number_previous']
            power_increase = user_record.loc[index,'power_increase']
            user_id = user

            record = Record(users, user_id, night_id,eeg,resp_x, resp_y, resp_z, time, time_previous, number_previous, power_increase)
            records[record_id] = record
        patient = Patient(user,records, record_ids)
        list_patients.append(patient)
        text = "\rUsers computed : {}% done".format(round(float(i) / len(users) * 100, 2))
        i += 1
        sys.stdout.write(text)
        sys.stdout.flush()
    return list_patients

def get_records_test(df):
    all_ids = np.unique(df['user'])
    eeg_cols = [col for col in df.columns if "eeg" in col]
    resp_x_cols = [col for col in df.columns if "respiration_x" in col]
    resp_y_cols = [col for col in df.columns if "respiration_y" in col]
    resp_z_cols = [col for col in df.columns if "respiration_z" in col]
    records= []
    for index, row in df.iterrows():
        user_id = row['user']
        night_id = row['night']
        time = row['time']
        time_previous = row['time_previous']
        number_previous = row['number_previous']
        eeg = row[eeg_cols].values.flatten()
        resp_x = row[resp_x_cols].values.flatten()
        resp_y = row[resp_y_cols].values.flatten()
        resp_z = row[resp_z_cols].values.flatten()
        record = Record(all_user_ids = all_ids, user_id = user_id, night_id=night_id, eeg=eeg, resp_x=resp_x, resp_y=resp_y, resp_z=resp_z, time=time, time_previous=time_previous, number_previous=number_previous)
        records.append(record)
        print("{}% done ".format(index/50000*100))
    return records



data = pd.read_csv(data_path)


# list_patients = get_patients_train(data)
# with open("train_data", 'wb') as output:
#     pickle.dump(list_patients, output, pickle.HIGHEST_PROTOCOL)

records = get_records_test(data)
with open("test_data", 'wb') as output:
     pickle.dump(records, output, pickle.HIGHEST_PROTOCOL)
