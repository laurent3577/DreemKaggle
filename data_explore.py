import numpy as np
import pickle
from scipy.signal import periodogram
from scipy.stats import pearsonr
from sklearn.preprocessing import StandardScaler
from spectrum import arburg
import pywt
import matplotlib.pyplot as plt
plt.style.use("seaborn-whitegrid")



def explore_data():
    with open("test_data", "rb") as input:
        data = pickle.load(input)

    user = data[np.random.randint(len(data))]
    record_index = user.record_ids[np.random.randint(len(user.record_ids))]
    record = user.records[record_index]
    print("Power increase : ", record.power_increase)


    t = np.linspace(0,8,2000)
    freq, spectral_density = periodogram(record.eeg, 250)
    freq_bands = np.array([[0, 0.5], [0.5, 1], [1, 1.5], [1.5, 2], [2, 2.5], [2.5, 3], [3.5, 4], [4, 8], [8, 12]])
    spectral_density_in_band = np.array([spectral_density[(band[0] <= freq) & (freq < band[1])].sum()/(band[1]-band[0]) for band in freq_bands])
    print("Features extracted : ", spectral_density_in_band)
    fig, axes = plt.subplots(2,1)
    axes[0].set_title("Periodogram for eeg with power increase {}".format(record.power_increase))
    axes[0].plot(freq, spectral_density)
    axes[1].set_title("EEG")
    axes[1].plot(t, record.eeg)
    plt.show()

    resp_x = record.resp_x
    wt = pywt.wavedec(resp_x, 'db4', level=5)
    print(wt, len(wt))
    return

def explore_features():
    with open("features", "rb") as input:
        [X, Y] = pickle.load(input)
    print("Features loaded")
    print("Normal scaling features")
    Y = StandardScaler().fit_transform(Y)
    X = StandardScaler().fit_transform(X)
    print("Performing Personr tests")
    for feature_index in range(X.shape[1]):
        correl_coeff, pval = pearsonr(X[:,feature_index],Y[:,0])
        print("Pearsonr test for feature {}".format(feature_index+1), "  : ", correl_coeff, "  ", pval, "   ", pval<0.05 )
    return
explore_data()
#explore_features()